/**
 *Compulsory
* You are given with x1, x2, y1, y2.
* x1, x2 represent the original location for both objects.
* y1, y2 represent the distance each object gets to move at one time.
* Your task is to complete the following function to indicate if both objects get to meet at the same location at the same time.
* Example 1:
* x1 = 0, y1 = 2
* x2 = 2, y2 = 1
* The first object will move from 0 and follow by 2, 4, 6, 8...
* The second object will move from 2 and follow by 3, 4, 5, 6...
* They met at 4, so the result should be true.
* Example 2:
* x1 = 0, y1 = 2
* x2 = 1, y2 = 3
* The first object will move from 0 and follow by 2, 4, 6, 8...
* The second object will move from 1 and follow by 4, 7, 10...
* They do not meet at the same time, so the result should be false.
 */


/**
 * 
 * @param {*} x1 original location of A
 * @param {*} x2 original location of B
 * @param {*} y1 distance of A to move
 * @param {*} y2 distance of B to move
 * 
 * return true or false
 */
function canObjectMeet(x1, x2, y1, y2){
    //Write your logic here
}
