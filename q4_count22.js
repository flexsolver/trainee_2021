/**
 * Bonus
 *
 * count 22
 * compute recursively the number of "22"
 *  substrings in the string. The "22" substrings should not overlap.
 * 
 * count22("22abc22") → 2
 * count22("abc22x22x22") → 3
 * count22("222") → 1
 *
 */

function count22(str) {
    /**
     * Your logic here
     */

    return 0;
}