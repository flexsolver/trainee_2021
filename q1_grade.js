/**
 * *Compulsory*
 * You are tasked to generate a grading system for Singapore-Cambridge GCE "O" Level Examination.
 * Input: Array of Integer, 
 * Output: Array of grade
 * 
 * You may refer to the following link or the attachement to know more about the grading system
 * https://www.singaporeeducation.info/education-system/grading-system.html
 * 
 * Example:
 * input: [80, 66, 52, 45]
 * output: [A1, B3, C6, F9]
 * 
 */


/**
 * 
 * @param {Array of Integer} input 
 */
function compute(input) {
    var output = new Array();
    /**
     * TODO HERE
     */


    return output;
}
